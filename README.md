# README #

Blockchain Seminar Project - https://courses.cs.ut.ee/2016/blockchain/fall/Main/Blockchain-project1


Andreas Ellervee

Orlenys López Pintado

## SETUP ##
PREQUISITES:

* npm (3.10.8)
* bower (1.6.8)
* Node (v6.9.1)
* testrpc (https://github.com/ethereumjs/testrpc)
* MongoDB (https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/#install-mongodb-community-edition)

First, the installation in /development folder.
```
#!bash
npm install
bower install
```
Second, database setup:
Once MongoDB is installed, it can be accessed from the command line using "mongo" command.
When in Mongo shell, "show dbs" will show all the available databases.
For our test-application, let's create test DB by running "use testdb" - this will create a new database.

Database URL is located in **app.js** *'mongodb://localhost:27017/testdb'* where "testdb" is the name of the database we have just created.

Then, if everything installed successfully

```
#!bash
# in one tab
testrpc
# in the second tab, in /development folder
npm start
```

Then open http://localhost:8080.