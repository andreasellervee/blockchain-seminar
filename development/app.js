
// Dependencies
var express = require('express');
var app = express();

var mongoose = require('mongoose');
var path = require('path');
var http = require('http');
var express = require('express');

var bodyParser = require('body-parser');

var port  	 = process.env.PORT || 8080;

// MongoDB Conection

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/mydb', function(error){
   if(error){
      throw error;
   }else{
      console.log('Conectado a MongoDB');
   }
});

app.use(express.static(__dirname + '/dist'));
app.use(bodyParser.json());

require('./app/routes.js')(app);
// Admins
require('./app/populateAdmins.js')(app);

app.listen(port);
