var app = angular.module('blockchainEscrow', ['ngRoute', 'ngResource', '720kb.datepicker']);

app.constant("Config", {
  "web3": new Web3()
})

app.controller('MainController', function($scope, $location, AuthenticationService, RegistrationService) {

  $scope.$watch(function watch() {
    return AuthenticationService.isLoggedIn();
  }, function changed(isLoggedIn) {
    $scope.isLoggedIn = isLoggedIn === 'true';
    if ($scope.isLoggedIn) {
      $scope.navbarLinks = [
        {name: "Properties", link: "properties"},
      ];
      if (sessionStorage.role === 'Admin') {
        $scope.navbarLinks.push({name: "Pending Requests", link: "requests/admin"});
        $scope.navbarLinks.push({name: "Previous Requests", link: "requests/past"});
        $scope.navbarLinks.push({name: "Register User", link: "registration"});
      } else if (sessionStorage.role === 'Host') {
         $scope.navbarLinks.push({name: "My Requests", link: "requests/host"});
         $scope.navbarLinks.push({name: "Add Property", link: "property_registration"});
      } else {
         $scope.navbarLinks.push({name: "My Requests", link: "requests/client"});
      }
    } else {
      $scope.navbarLinks = [
        {name: "Log In", link: "authentication"},
        {name: "Register", link: "registration"}
      ];
    }
  });

  $scope.logOut = function () {
    AuthenticationService.logOut();
    $location.path('/');
  }

  $scope.getAccountInformation = function() {
    return "User: " + sessionStorage.username + ", Role: " + sessionStorage.role;
  }
});

app.config(function($routeProvider, Config){
  if (!Config.web3.currentProvider) {
      Config.web3.setProvider(new Config.web3.providers.HttpProvider("http://localhost:8545"));
  }
  $routeProvider
      .when('/', {
        templateUrl: 'views/homepage.html',
        controller: 'HomeController',
        controllerAs: 'home'
      })
      .when('/authentication', {
        templateUrl: 'views/authentication.html',
        controller: 'AuthenticationController',
        controllerAs: 'auth'
      })
      .when('/registration', {
        templateUrl: 'views/registration.html',
        controller: 'RegistrationController',
        controllerAs: 'reg'
      })
      .when('/property_registration', {
        templateUrl: 'views/propertyRegistration.html',
        controller: 'PropertyRegController',
        controllerAs: 'regProp'
      })
      .when('/contract_test', {
        templateUrl: 'views/frontpage.html',
        controller: 'FirstController',
        controllerAs: 'first'
      })
      .when('/requests/:access', {
        templateUrl: 'views/requests.html',
        controller: 'RequestsController',
        controllerAs: 'requests',
        resolve: {
          myRequests: function($route, RequestsService) {
            return RequestsService.getContracts($route.current.params.access, sessionStorage.username).$promise;
          }
        }
      })
      .when('/request/:id', {
        templateUrl: 'views/request.html',
        controller: 'RequestController',
        controllerAs: 'request',
        resolve: {
          request: function($route, RequestsService) {
            return RequestsService.getContract($route.current.params.id).$promise;
          }
        }
      })
      .when('/properties', {
        templateUrl: 'views/properties.html',
        controller: 'PropertiesController',
        controllerAs: 'propsCtrl',
        resolve: {
          properties: function(PropertiesService) {
            return PropertiesService.getProperties().$promise;
          }
        }
      })
      .when('/property/:id', {
        templateUrl: 'views/property.html',
        controller: 'PropertyController',
        controllerAs: 'propCtrl',
        resolve: {
          property: function($route, PropertiesService) {
            return PropertiesService.getProperty($route.current.params.id).$promise;
          }
        }
      });
});
