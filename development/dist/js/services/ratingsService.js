'use strict';

angular.module('blockchainEscrow').service('RatingsService', ratingsService);

function ratingsService($resource) {
  var Rating = $resource('/api/rating/:property_id');

  return {
    getRating: getRating,
    addRating: addRating
  }

  function getRating(property_id) {
    return Rating.query({property_id: property_id});
  }

  function addRating(property_id, username, rating) {
    return Rating.save({
      property_id: property_id,
      username: username,
      rating: rating
    })
  }

}
