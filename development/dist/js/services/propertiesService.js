'use strict';

angular.module('blockchainEscrow').service('PropertiesService', propertiesService);

function propertiesService($resource) {
  var PropertiesList = $resource('/api/property-list');
  var Properties = $resource('/api/property/:id', null, {
    'update': { method:'PUT' }
  });

  return {
    getProperties: getProperties,
    getProperty: getProperty,
    updateProperty: updateProperty,
    removeProperty: removeProperty
  };

  function getProperties() {
    return PropertiesList.query();
  }

  function getProperty(id) {
    return Properties.get({id: id});
  }

  function updateProperty(property) {
    return Properties.update({id: property._id}, property);
  }

  function removeProperty(id){
    return Properties.delete({id: property._id});
  }
}
