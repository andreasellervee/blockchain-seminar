'use strict';

angular.module('blockchainEscrow').service('AuthenticationService', authenticationService);

function authenticationService(RegistrationService) {

  return {
    logOut: logOut,
    isLoggedIn: isLoggedIn
  }

  function logOut() {
    sessionStorage.authenticated = false;
    sessionStorage.username = '';
    sessionStorage.fullName = '';
    sessionStorage.email = '';
    sessionStorage.cellphone = '';
    sessionStorage.password = '';
    sessionStorage.role = '';
    sessionStorage.user_id = '';
  }

  function isLoggedIn() {
    return sessionStorage.authenticated;
  }

}
