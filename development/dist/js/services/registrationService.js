'use strict';

angular.module('blockchainEscrow').service('RegistrationService', registrationService);

function registrationService($http, $location, $q) {

  return {
    createUser: createUser,
    loadUsers: loadUsers,
    loadUser: loadUser,
    removeUser: removeUser
  };

  function createUser(user) {
    $http({
        method: 'POST',
        url: '/create_user',
        params: {
            fullName: user.fullName,
            username: user.username,
            pasword: user.pasword,
            email: user.email,
            cellphone: user.cellphone,
            role: user.role
        }
      }).success(function(data) {
            if(typeof(data) == 'object'){
              console.log('The user ' + user.username + ' was added');
            }else{
                alert('Error while creating the user.');
            }
        }).
        error(function() {
            alert('Error while saving the user.');
        });
  }

  function loadUsers(){
      $http({
          method: 'GET', url: '/users'
      }).
      success(function(data) {
          if(typeof(data) == 'object'){
              return data;
          }else{
              return null;
          }
      }).
      error(function() {
          return null;
      });
  }

  function loadUser(userName) {
      var defered = $q.defer();
      var promise = defered.promise;
      $http({
          method: 'GET',
          url: '/user',
          params: {
              username: userName
          }
      })
      .success(function(data) {
        console.log(data);
        if(data.length > 1){
          console.log("01");
          var userQ = {
            username: data[0].username,
            password: data[0].password,
            fullName: data[1].fullName,
            email: data[1].email,
            cellphone: data[1].cellphone,
            role: data[1].role
          };
          defered.resolve(userQ);
        } else {
           console.log("01");
           defered.reject(data);
        }
      })
      .error(function(err) {
          console.log("01");
          defered.reject(err);
      });
      return promise;
  }

  function removeUser(userName) {
      $http({
          method: 'POST',
          url: '/remove_user',
          params: {
              username: userName
          }
      }).
      success(function(data) {
          if(data == 'Ok'){

          }else{
              alert('Error while removing a user.');
          }
      }).
      error(function() {
          alert('Error while removing a user.');
      });
  }
}
