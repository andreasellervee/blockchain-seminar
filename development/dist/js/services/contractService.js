'use strict';

angular.module('blockchainEscrow').service('ContractService', contractService);

function contractService(Config) {
  var web3 = Config.web3;

  var contractInteface = [{"constant":false,"inputs":[],"name":"blockHostDeposit","outputs":[],"payable":true,"type":"function"},{"constant":false,"inputs":[],"name":"cancelBeforeClientTransfers","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"blockClientDeposit","outputs":[],"payable":true,"type":"function"},{"constant":false,"inputs":[],"name":"userCancelation","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"hostCancelation","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_rentalPrice","type":"uint256"}],"name":"setRentalPrice","outputs":[],"payable":true,"type":"function"},{"constant":false,"inputs":[],"name":"processContractConclusion","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"processClientViolation","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"cancelBeforeHostTransfers","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"processContractTransaction","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"processHostViolation","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"processTimeEvent","outputs":[],"payable":false,"type":"function"}];

  return {
    initWeb3: initWeb3,
    createContract: createContract,
    cancelBeforeClientTransfers: cancelBeforeClientTransfers,
    blockClientDeposit: blockClientDeposit,
    cancelBeforeHostTransfers: cancelBeforeHostTransfers,
    blockHostDeposit: blockHostDeposit,
    setRentalPrice: setRentalPrice,
    clientCancelAfter: clientCancelAfter,
    hostCancelAfter: hostCancelAfter,
    processTimeEvent: processTimeEvent,
    processContractTransaction: processContractTransaction,
    processClientViolation: processClientViolation,
    processHostViolation: processHostViolation,
    sendDeposit: sendDeposit
  };

  function initWeb3() {
    if (!web3.currentProvider) {
        web3.setProvider(new web3.providers.HttpProvider("http://localhost:8545"));
    }
  }

  function createContract(hostAddress, contractCallbackFn) {
    var mycontract = web3.eth.contract(contractInteface).new({
      from: hostAddress,
      data: '0x6060604052600160055534610000575b6109a38061001e6000396000f300606060405236156100b8576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680630207f8bf146100bd57806304f2586b146100c75780630bd04cde146100ee578063176d38f5146100f85780631bf03395146101075780633d4094e5146101165780633ff597bc1461012e5780635a5bcce11461013d5780639864d33b1461014c5780639f48a2cf14610173578063c653cfe914610182578063eb02aa6714610191575b610000565b6100c56101a0565b005b34610000576100d4610251565b604051808215151515815260200191505060405180910390f35b6100f66102ce565b005b346100005761010561036d565b005b3461000057610114610408565b005b61012c600480803590602001909190505061049f565b005b346100005761013b61051a565b005b346100005761014a610633565b005b346100005761015961065e565b604051808215151515815260200191505060405180910390f35b3461000057610180610742565b005b346100005761018f6107ed565b005b346100005761019e61087e565b005b600360055414158061020057503373ffffffffffffffffffffffffffffffffffffffff16600160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1614155b8061022b5750346002026004541415801561022a5750670de0b6b3a7640000346002020160045414155b5b1561023557610000565b346003819055506005600081548092919060010191905055505b565b600060026005541415806102b35750600160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614155b156102bd57610000565b600060048190556005819055505b90565b6002600554141580610306575034600202600454600302141580156103055750670de0b6b3a7640000346002020160045460030214155b5b1561031057610000565b33600060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550346002819055506005600081548092919060010191905055505b565b60046005541415806103cd5750600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614155b156103d757610000565b6104056002600354811561000057046002540360026003548115610000570460035401600060006000610893565b5b565b60046005541415806104685750600160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614155b1561047257610000565b61049c60026003548115610000570460025401600260035481156100005704600060006000610893565b5b565b60016005541415806104b2575060008111155b156104bc57610000565b33600160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550806004819055506005600081548092919060010191905055505b50565b60066005541415806105d45750600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141580156105d35750600160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614155b5b156105de57610000565b3373ffffffffffffffffffffffffffffffffffffffff166108fc6002549081150290604051809050600060405180830381858888f19350505050151561062357610000565b600060028190556005819055505b565b600560055414151561064457610000565b61065b600454600354600202600060006000610893565b5b565b600060036005541415806106c05750600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614155b156106ca57610000565b600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166108fc6002549081150290604051809050600060405180830381858888f19350505050151561073157610000565b600060028190556005819055505b90565b600560055414151561075357610000565b600160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166108fc600354600454019081150290604051809050600060405180830381858888f1935050505015156107be57610000565b60045460026000828254039250508190555060006003819055506005600081548092919060010191905055505b565b60056005541415156107fe57610000565b600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166108fc600354600254019081150290604051809050600060405180830381858888f19350505050151561086957610000565b6000600281905560038190556005819055505b565b6005600081548092919060010191905055505b565b600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166108fc869081150290604051809050600060405180830381858888f1935050505015806109505750600160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166108fc859081150290604051809050600060405180830381858888f19350505050155b1561095a57610000565b8160038190555082600281905550806005819055505b50505050505600a165627a7a72305820b928ecf4ae117745f985f54ee1c236aa7d663c6eba44cf0f98a82b3dab70729b0029',
      gas: '4700000'
    }, contractCallbackFn)
  }

  function setRentalPrice(hostAddress, contractAddress, _rentalPrice) {
    console.log("setRentalPrice executed ...");
    web3.eth.contract(contractInteface)
            .at(contractAddress)
            .setRentalPrice(new BigNumber(_rentalPrice), {
              from: hostAddress
            });
  }

  function blockClientDeposit(contractAddress, clientAddress, _clientPrice) {
    console.log("blockClientDeposit executed ...");
    web3.eth.contract(contractInteface)
            .at(contractAddress)
            .blockClientDeposit({
              from: clientAddress, value: new BigNumber(_clientPrice)
            });
  }

  function blockHostDeposit(contractAddress, hostAddress, _hostPrice) {
    console.log("blockHostDeposit executed ...");
    web3.eth.contract(contractInteface)
            .at(contractAddress)
            .blockHostDeposit({
              from: hostAddress, value: new BigNumber(_hostPrice)
            });
  }

  function cancelBeforeHostTransfers(contractAddress, clientAddress) {
    console.log("cancelBeforeHostTransfers executed ...");
    web3.eth.contract(contractInteface)
            .at(contractAddress)
            .cancelBeforeHostTransfers({from: clientAddress});
  }

  function cancelBeforeClientTransfers(contractAddress, hostAddress) {
    console.log("cancelBeforeClientTransfers executed ...");
    web3.eth.contract(contractInteface)
            .at(contractAddress)
            .cancelBeforeClientTransfers({from: hostAddress});
  }

  function clientCancelAfter(contractAddress, clientAddress) {
    console.log("clientCancelAfter executed ...");
    web3.eth.contract(contractInteface)
            .at(contractAddress)
            .userCancelation({from: clientAddress});
  }

  function hostCancelAfter(contractAddress, hostAddress) {
    console.log("hostCancelAfter executed ...");
    web3.eth.contract(contractInteface)
            .at(contractAddress)
            .hostCancelation({from: hostAddress});
  }

  function processTimeEvent(contractAddress, hostAddress) {
    console.log("processTimeEvent executed ...");
    web3.eth.contract(contractInteface)
            .at(contractAddress)
            .processTimeEvent({from: hostAddress});
  }

  function processContractTransaction(contractAddress, hostAddress) {
     console.log("processContractTransaction executed ...");
     web3.eth.contract(contractInteface)
            .at(contractAddress)
            .processContractTransaction({from: hostAddress});
  }

  function processClientViolation(contractAddress, clientAddress) {
     console.log("processClientViolation executed ...");
     web3.eth.contract(contractInteface)
           .at(contractAddress)
           .processClientViolation({from: clientAddress});
  }

  function processHostViolation(contractAddress, hostAddress){
    console.log("processHostViolation executed ...");
     web3.eth.contract(contractInteface)
          .at(contractAddress)
          .processHostViolation({from: hostAddress});
  }

  function sendDeposit(contractAddress, userAddress){
     console.log("sendDeposit executed ...");
     web3.eth.contract(contractInteface)
            .at(contractAddress)
            .processContractConclusion({from: userAddress});
  }

}
