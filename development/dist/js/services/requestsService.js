'use strict';

angular.module('blockchainEscrow').service('RequestsService', requestsService);

function requestsService($resource) {
  var Requests = $resource('/requests/:id');
  var Contract = $resource('/api/contract/:id', null, {
    'update': { method:'PUT' }
  });
  var ClientContracts = $resource('/api/contract_client/:client_username');
  var HostContracts = $resource('/api/contract_host/:host_username');
  var PendingContracts = $resource('/api/contract-list/:enabled');
  var PreviousContracts = $resource('/api/contract-list/:enabled');

  return {
    getRequests: getRequests,
    getRequest: getRequest,
    createContract: createContract,
    getContract: getContract,
    updateContract: updateContract,
    getContracts: getContracts,
    removeContract: removeContract
  };

  function getRequests() {
    return Requests.query();
  }

  function getRequest(id) {
    return Requests.get({id: id});
  }

  function getContract(id) {
    return Contract.get({id: id});
  }

  function createContract(contract) {
    return Contract.save(contract);
  }

  function updateContract(contract) {
    return Contract.update({id: contract._id}, contract);
  }

  function removeContract(id) {
    Contract.delete({id: id});
  }

  function getContracts(access, username) {
    if (access.toLowerCase() == "client") {
      return ClientContracts.query({client_username: username});
    } else if (access.toLowerCase() == "host"){
      return HostContracts.query({host_username: username});
    } else if (access.toLowerCase() == "admin"){
      return PendingContracts.query({enabled: true});
    } else {
      return PreviousContracts.query({enabled: false});
    }
  }
}
