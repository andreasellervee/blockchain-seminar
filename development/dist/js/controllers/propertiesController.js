'use strict';

angular.module('blockchainEscrow').controller('PropertiesController', propertiesController);

function propertiesController(properties, $http, $scope, $location, PropertiesService) {
  var vm = this;

  vm.message = 'For selecting a property, just click on it.';
  vm.properties = properties;
  if (vm.properties.length == 0) {
    vm.message = 'There are not properties to show.';
  }

  vm.selectProperty = selectProperty;
  vm.getRating = getRating;

  function selectProperty(property) {
    if (sessionStorage.authenticated) {
      $location.path('/property/' + property._id);
    }
	};

  function getRating(ratings) {
    if (_.isEmpty(ratings)) {
      return 'Not rated';
    } else {
      return (_.sum(_.map(ratings, 'rating')) / ratings.length).toFixed(2);
    }
  }

}
