'use strict';

angular.module('blockchainEscrow').controller('FirstController', firstController);

function firstController($scope, Config, ContractService) {
  // short for viewModel, we don't have to use $scope when using this notation (controllerAs).
  var vm = this;

  // read web3 from Config.
  var web3 = Config.web3;

  // host creates the contract
  vm.hostAddress = web3.eth.accounts[0];
  vm.hostBalance = getBalanceInEther(vm.hostAddress);
  vm.contractAddress = "Contract not yet created.";
  vm.contractDate = new Date();

  // mock client
  vm.clientAddress = web3.eth.accounts[1];
  vm.clientBalance = getBalanceInEther(vm.clientAddress);

  // functions
  vm.createContract = createContract;
  vm.blockClientDeposit = blockClientDeposit;
  vm.clientCancel = clientCancel;
  vm.blockHostDeposit = blockHostDeposit;
  vm.clientCancelAfter = clientCancelAfter;
  vm.hostCancelAfter = hostCancelAfter;
  vm.invalidateAfterFirstTimeEvent = invalidateAfterFirstTimeEvent;
  vm.invalidateAfterSecondTimeEvent = invalidateAfterSecondTimeEvent;
  vm.processClientViolation = processClientViolation;
  vm.processHostViolation = processHostViolation;
  vm.sendDepositToClient = sendDepositToClient;
  vm.sendDepositToHost = sendDepositToClient;

  // implementation of functions
  function createContract() {
    ContractService.createContract(vm.hostAddress, contractCallback);
  }

  function blockClientDeposit() {
    var clientPrice = web3.toWei(parseInt(vm.clientPrice), 'ether');
    ContractService.blockClientDeposit(vm.contractAddress,
                                       vm.clientAddress,
                                       clientPrice);
    vm.clientBalance = getBalanceInEther(vm.clientAddress);
  }

  // Step 3.1
  function clientCancel() {
    ContractService.cancelBeforeHostTransfers(vm.contractAddress, vm.clientAddress);
    vm.clientBalance = getBalanceInEther(vm.clientAddress);
  }

  // Step 3.2
  function blockHostDeposit() {
    var hostPrice = web3.toWei(parseInt(vm.hostPrice), 'ether');
    ContractService.blockHostDeposit(vm.contractAddress,
                                     vm.hostAddress,
                                     hostPrice);
    vm.hostBalance = getBalanceInEther(vm.hostAddress);
    vm.firstTimeEvent = setTimeout(invalidateAfterFirstTimeEvent, 30000);
  }


  function invalidateAfterFirstTimeEvent(){
    ContractService.processTimeEvent(vm.contractAddress, vm.hostAddress);
    vm.secondTimeEvent = setTimeout(invalidateAfterSecondTimeEvent, 30000);
    alert('Deadline to cancel the contract fulfilled.');
  }

  function invalidateAfterSecondTimeEvent(){
     ContractService.processContractTransaction(vm.contractAddress, vm.hostAddress);
     alert('Appointment deadline.');
     vm.clientBalance = getBalanceInEther(vm.clientAddress);
     vm.hostBalance = getBalanceInEther(vm.hostAddress);
  }

  function processClientViolation(){
     clearTimeout(vm.secondTimeEvent);
     ContractService.processClientViolation(vm.contractAddress, vm.clientAddress);
     vm.clientBalance = getBalanceInEther(vm.clientAddress);
     vm.hostBalance = getBalanceInEther(vm.hostAddress);
  }

  function processHostViolation(){
    clearTimeout(vm.secondTimeEvent);
    ContractService.processHostViolation(vm.contractAddress, vm.hostAddress);
    vm.clientBalance = getBalanceInEther(vm.clientAddress);
    vm.hostBalance = getBalanceInEther(vm.hostAddress);
  }

  function sendDepositToClient(){
    ContractService.sendDeposit(vm.contractAddress, vm.clientAddress);
    vm.clientBalance = getBalanceInEther(vm.clientAddress);
    vm.hostBalance = getBalanceInEther(vm.hostAddress);
  }

  function sendDepositToHost(){
    ContractService.sendDeposit(vm.contractAddress, vm.hostAddress);
    vm.clientBalance = getBalanceInEther(vm.clientAddress);
    vm.hostBalance = getBalanceInEther(vm.hostAddress);
  }

  function clientCancelAfter(){
    clearTimeout(vm.firstTimeEvent);
    ContractService.clientCancelAfter(vm.contractAddress, vm.clientAddress);
    vm.clientBalance = getBalanceInEther(vm.clientAddress);
    vm.hostBalance = getBalanceInEther(vm.hostAddress);
  }

  function hostCancelAfter(){
    clearTimeout(vm.firstTimeEvent);
    ContractService.hostCancelAfter(vm.contractAddress, vm.hostAddress);
    vm.hostBalance = getBalanceInEther(vm.hostAddress);
    vm.clientBalance = getBalanceInEther(vm.clientAddress);
  }


  function contractCallback(error, contract) {
    if (!error) {
      // This is triggered 2 times, according to documentation
      if (contract.address) {
          vm.contractAddress = contract.address;
          vm.hostBalance = getBalanceInEther(vm.hostAddress);
          // Set the price
          var rentalPrice = web3.toWei(parseInt(vm.rentalPrice), 'ether');
          ContractService.setRentalPrice(vm.hostAddress,
                                         contract.address,
                                         rentalPrice);
          $scope.$apply();
      }
    } else {
      console.error(error);
    }
  }

  // helper function to convert Wei to Ether
  function getBalanceInEther(address) {
    return web3.fromWei(web3.eth.getBalance(address), 'ether');
  }
}
