'use strict';

angular.module('blockchainEscrow').controller('AuthenticationController', authenticationController);

function authenticationController($http, $scope, $location) {
  var vm = this;

  $scope.user = {};

  vm.login = function() {
    console.log($scope.username);
    $http.get('/api/user/' + $scope.user.username).success(function(data) {
      if (data[0] && data[0].username === $scope.user.username && data[0].password === $scope.user.password){
        sessionStorage.authenticated = true;
        sessionStorage.username = data[0].username;
        sessionStorage.fullName = data[0].fullName;
        sessionStorage.email = data[0].email;
        sessionStorage.cellphone = data[0].cellphone;
        sessionStorage.password = data[0].password;
        sessionStorage.role = data[0].role;
        sessionStorage.user_id = data[0]._id;
        $location.path('/');
      } else {
         alert('Invalid username or password');
      }
  	})
  	.error(function(data) {
  		 alert('The username does not exist in the database');
  	});
  }
}
