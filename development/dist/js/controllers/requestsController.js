'use strict';

angular.module('blockchainEscrow').controller('RequestsController', requestsController);

function requestsController(myRequests, $location) {
  var vm = this;
  vm.getRowColor = getRowColor;

  var cancellations = [
    'Rejected by host',
    'Cancelled by host',
    'Cancelled by client',
    'Cancelled by client',
    'Cancelled with fee by host',
    'Cancelled with fee by client',
    'Cancelled by admin (Host violation)',
    'Cancelled by admin (Client violation)'
  ];

  vm.myActiveRequests = [];
  vm.myPastRequests = [];

  _.forEach(myRequests, function iterate(request) {
    if (_.includes(cancellations, request.status)
      || _.isEqual(request.status, 'Contract completed')) {
      vm.myPastRequests.push(request);
    } else {
      vm.myActiveRequests.push(request);
    }
  })

  function getRowColor(r) {
     if (cancellations.indexOf(r.status) >= 0) {
       return 'danger';
     } else if (r.status == 'Contract completed') {
       return 'success';
     } else {
       return '';
     }
  }
}
