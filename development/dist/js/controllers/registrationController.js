'use strict';

angular.module('blockchainEscrow').controller('RegistrationController', registrationController);

function registrationController($location, $http, $scope) {
  var vm = this;

  $scope.user = {};
  $scope.users = {};
  $scope.roles = ['Client', 'Host'];

  if(sessionStorage.role === 'Admin')
    $scope.roles = ['Client', 'Host', 'Admin'];

  vm.saveUser = function(){
    $http.post('/api/user', $scope.user)
    .success(function(data) {
				$scope.user = {};
				$scope.users = data;
        console.log(data);
        $location.path('/');
			})
		.error(function(data) {
			console.log('Error: ' + data);
		});
  }
}
