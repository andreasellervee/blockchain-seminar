'use strict';

angular.module('blockchainEscrow').controller('RequestController', requestController);

function requestController(Config, request, $location, PropertiesService, RequestsService, ContractService, RatingsService) {
  var vm = this;
  // read web3 from Config.
  var web3 = Config.web3;
  vm.request = request;
  // Ratings
  vm.rating = 0;
  vm.userHasAlreadyRated = false;
  vm.ratingText = "Please rate this property !";

  vm.isHost = sessionStorage.role === 'Host';
  vm.isClient = sessionStorage.role === 'Client';
  vm.isAdmin = sessionStorage.role === 'Admin';
  vm.hostCanAcceptOrReject = hostCanAcceptOrReject;
  vm.waitingForClientPayment = waitingForClientPayment;
  vm.clientPaymentDone = clientPaymentDone;
  vm.waitingForFeeCancelation = waitingForFeeCancelation;
  vm.waitingForRentalMeeting = waitingForRentalMeeting;
  vm.finalMeeting = finalMeeting;
  vm.contractCompleted = contractCompleted;
  vm.getBalance = getBalance;
  var user = sessionStorage.role === 'Client' ? 'client' : 'host';

  RatingsService.getRating(vm.request.property).$promise.then(function success(ratings) {
    if (_.includes(_.map(ratings, 'username'), sessionStorage.username)) {
      vm.rating = _.find(ratings, {username: sessionStorage.username}).rating;
      vm.userHasAlreadyRated = true;
      vm.ratingText = "You rated this property with: " + vm.rating;
    }
  })

  function getBalance() {
     var ad = vm.isHost ? request.hostAddress : request.clientAddress;
     if (ad !== 'Pending')
        return getBalanceInEther(ad);
     return 0;
  }

  function hostCanAcceptOrReject() {
    return vm.request.status === "Created by client";
  }

  function waitingForClientPayment() {
    return vm.request.status === "Waiting for client payment";
  }

  function clientPaymentDone() {
    return vm.request.status === "Waiting for host payment";
  }

  function waitingForFeeCancelation(){
    return vm.request.status === "Possible cancelation with 50% of penalty";
  }

  function waitingForRentalMeeting() {
    return vm.request.status === "Waiting for rental meeting";
  }

  function finalMeeting() {
    return vm.request.status === "Client in property";
  }

  function contractCompleted() {
    return vm.request.status === "Contract completed";
  }

  vm.acceptProposal = acceptProposal;
  vm.rejectProposal = rejectProposal;
  vm.prematureHostCancel = prematureHostCancel;
  vm.makeClientPayment = makeClientPayment;
  vm.prematureCancel = prematureCancel;
  vm.makeHostPayment = makeHostPayment;
  vm.invalidateAfterFirstTimeEvent = invalidateAfterFirstTimeEvent;
  vm.hostCancelWithFee = hostCancelWithFee;
  vm.clientCancelWithFee = clientCancelWithFee;
  vm.skipToCheckoutDate = skipToCheckoutDate;
  vm.rentalCompleteOk = rentalCompleteOk;
  vm.rentalCompleteWithoutDeposit = rentalCompleteWithoutDeposit;
  vm.hostViolateAgreement = hostViolateAgreement;
  vm.clientViolateAgreement = clientViolateAgreement;
  vm.addRating = addRating;

  function acceptProposal() {
    if (!web3.isAddress(vm.request.hostAddress)) {
      alert("Not a valid blockchain address");
      return;
    }
    ContractService.createContract(vm.request.hostAddress, function cb(error, contract) {
      if (!error) {
        // This is triggered 2 times, according to documentation
        if (contract.address) {
            vm.request.contractAddress = contract.address;
            PropertiesService.getProperty(vm.request.property).$promise.then(function (prop){
              // Set the price, should be calculated by rental period in days * property cost.
              var rentalPrice = web3.toWei(parseInt(prop.cost), 'ether');
              ContractService.setRentalPrice(vm.request.hostAddress,
                                             vm.request.contractAddress,
                                             rentalPrice);
            })
            vm.request.status = "Waiting for client payment";
            RequestsService.updateContract(vm.request);
            $location.path("/requests/" + user);
        }
      } else {
        alert("Error creating contract !");
      }
    })
  }

  function rejectProposal() {
    PropertiesService.getProperty(vm.request.property).$promise.then(function(property) {
      property.reserved = false;
      PropertiesService.updateProperty(property);
      vm.request.status = "Rejected by host";
      vm.request.available = false;
      RequestsService.updateContract(vm.request);
      $location.path("/requests/" + user);
    });
  }

  function prematureHostCancel() {
    PropertiesService.getProperty(vm.request.property).$promise.then(function (prop){
      ContractService.cancelBeforeClientTransfers(vm.request.contractAddress, vm.request.hostAddress);
      prop.reserved = false;
      PropertiesService.updateProperty(prop);
      vm.request.status = "Cancelled by host";
      vm.request.available = false;
      RequestsService.updateContract(vm.request);
      $location.path("/requests/" + user);
    })
  }

  function makeClientPayment() {
    if (!web3.isAddress(vm.request.clientAddress)) {
      alert("Not a valid blockchain address");
      return;
    }
    PropertiesService.getProperty(vm.request.property).$promise.then(function (prop){
      var clientPrice = web3.toWei(parseInt(prop.cost * 1.5), 'ether');
      ContractService.blockClientDeposit(vm.request.contractAddress, vm.request.clientAddress, clientPrice);
      vm.request.status = "Waiting for host payment";
      RequestsService.updateContract(vm.request);
      $location.path("/requests/" + user);
    })
  }

  function prematureCancel() {
    PropertiesService.getProperty(vm.request.property).$promise.then(function (prop){
      ContractService.cancelBeforeHostTransfers(vm.request.contractAddress, vm.request.clientAddress);
      prop.reserved = false;
      PropertiesService.updateProperty(prop);
      vm.request.status = "Cancelled by client";
      vm.request.available = false;
      RequestsService.updateContract(vm.request);
      $location.path("/requests/" + user);
    })
  }

  function makeHostPayment() {
    PropertiesService.getProperty(vm.request.property).$promise.then(function (prop){
      var hostPrice = web3.toWei(parseInt(prop.cost * 0.5), 'ether');
      console.log(hostPrice);
      console.log(web3.toWei(parseInt(prop.cost), 'ether'));
      ContractService.blockHostDeposit(vm.request.contractAddress, vm.request.hostAddress, hostPrice);
      vm.request.status = "Possible cancelation with 50% of penalty";
      RequestsService.updateContract(vm.request);
      vm.firstTimeEvent = setTimeout(invalidateAfterFirstTimeEvent, 10000);
      $location.path("/requests/" + user);
    })
  }

  function hostCancelWithFee() {
    PropertiesService.getProperty(vm.request.property).$promise.then(function (prop) {
      ContractService.hostCancelAfter(vm.request.contractAddress, vm.request.hostAddress);
      prop.reserved = false;
      PropertiesService.updateProperty(prop);
      vm.request.status = "Cancelled with fee by host";
      vm.request.available = false;
      RequestsService.updateContract(vm.request);
      $location.path("/requests/" + user);
    })
  }

  function clientCancelWithFee() {
    PropertiesService.getProperty(vm.request.property).$promise.then(function (prop) {
      ContractService.clientCancelAfter(vm.request.contractAddress, vm.request.clientAddress);
      prop.reserved = false;
      PropertiesService.updateProperty(prop);
      vm.request.status = "Cancelled with fee by client";
      vm.request.available = false;
      RequestsService.updateContract(vm.request);
      $location.path("/requests/" + user);
    })
  }

  function skipToCheckoutDate() {
    ContractService.processContractTransaction(vm.request.contractAddress, vm.request.hostAddress);
    vm.request.status = "Client in property";
    RequestsService.updateContract(vm.request);
  }

  function hostViolateAgreement() {
    clearTimeout(vm.firstTimeEvent);
    PropertiesService.getProperty(vm.request.property).$promise.then(function (prop) {
      ContractService.processHostViolation(vm.request.contractAddress, vm.request.hostAddress);
      prop.reserved = false;
      PropertiesService.updateProperty(prop);
      vm.request.status = "Cancelled by admin (Host violation)";
      vm.request.available = false;
      RequestsService.updateContract(vm.request);
    })
  }

    function clientViolateAgreement() {
      clearTimeout(vm.firstTimeEvent);
      PropertiesService.getProperty(vm.request.property).$promise.then(function (prop) {
        ContractService.processClientViolation(vm.request.contractAddress, vm.request.clientAddress);
        prop.reserved = false;
        PropertiesService.updateProperty(prop);
        vm.request.status = "Cancelled by admin (Client violation)";
        vm.request.available = false;
        RequestsService.updateContract(vm.request);
      })
  }

  function rentalCompleteOk() {
    ContractService.sendDeposit(vm.request.contractAddress, vm.request.clientAddress);
    vm.request.status = "Contract completed";
    vm.request.available = false;
    RequestsService.updateContract(vm.request);
    PropertiesService.getProperty(vm.request.property).$promise.then(function (prop) {
      prop.reserved = false;
      PropertiesService.updateProperty(prop);
      $location.path("/requests/" + user);
    });
  }

  function rentalCompleteWithoutDeposit() {
    ContractService.sendDeposit(vm.request.contractAddress, vm.request.hostAddress);
    vm.request.status = "Contract completed";
    vm.request.available = false;
    RequestsService.updateContract(vm.request);
    PropertiesService.getProperty(vm.request.property).$promise.then(function (prop) {
      prop.reserved = false;
      PropertiesService.updateProperty(prop);
      $location.path("/requests/" + user);
    });
  }

  function invalidateAfterFirstTimeEvent(){
    ContractService.processTimeEvent(vm.request.contractAddress, vm.request.hostAddress);
    vm.request.status = "Waiting for rental meeting";
    RequestsService.updateContract(vm.request);
    alert('Deadline to cancel the contract fulfilled.');
  }

  function getBalanceInEther(address) {
    return web3.fromWei(web3.eth.getBalance(address), 'ether');
  }

  function addRating(r) {
    vm.rating = parseInt(r);
    vm.userHasAlreadyRated = true;
    RatingsService.addRating(vm.request.property, sessionStorage.username, vm.rating);
  }

  vm.ratingsRange = _.range(1,6);
  vm.starMouseOver = function(r) {
    if (!vm.userHasAlreadyRated) {
      vm.rating = parseInt(r);
    }
  }
  vm.getStarClass = function(r) {
    if (parseInt(r) <= vm.rating) {
      return 'glyphicon glyphicon-star';
    } else {
      return 'glyphicon glyphicon-star-empty';
    }
  }

}
