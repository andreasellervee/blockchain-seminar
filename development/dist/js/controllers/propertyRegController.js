'use strict';

angular.module('blockchainEscrow').controller('PropertyRegController', propertyRegController);

function propertyRegController($scope, $http, $location) {
  var vm = this;

  $scope.property = {};

  vm.saveProperty = function(){
    $scope.property.host = sessionStorage.username;
    $scope.property.reserved = false;    
    $http.post('/api/property', $scope.property)
    .success(function(data) {
				$scope.property = {};
				$scope.property = data;
        console.log(data);
        $location.path('/');
			})
		.error(function(data) {
			console.log('Error: ' + data);
		});
  }

}
