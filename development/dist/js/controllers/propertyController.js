'use strict';

angular.module('blockchainEscrow').controller('PropertyController', propertyController);

function propertyController(property, $location, RequestsService, PropertiesService, RatingsService) {
  var vm = this;

  vm.isClient = sessionStorage.role === 'Client';
  vm.property = property;
  vm.contract = {
    client: sessionStorage.username,
    host: vm.property.host,
    property: vm.property._id,
    status: 'Created by client',
    contractAddress: 'Pending',
    clientAddress: 'Pending',
    hostAddress: 'Pending',
    meetingDate: '',
    startDate: '',
    endDate: '',
    available: true
  };

  vm.rating = 'Not rated';

  RatingsService.getRating(vm.property._id).$promise.then(function success(ratings) {
    if (!_.isEmpty(ratings)) {
      vm.rating = (_.sum(_.map(ratings, 'rating')) / ratings.length).toFixed(2);
    }
  });

  vm.sendProposal = function(){
    if (!validProposalDates()) {
      alert('Invalid dates for contract');
    } else {
      RequestsService.createContract(vm.contract).$promise.then(function (createdContract) {
        vm.property.reserved = true;
        PropertiesService.updateProperty(vm.property);
        $location.path('/requests/client');
      })
    }
  }

  function validProposalDates() {
    return new Date(vm.contract.startDate) < new Date(vm.contract.endDate)
      && new Date(vm.contract.meetingDate) <= new Date(vm.contract.startDate);
  }

}
