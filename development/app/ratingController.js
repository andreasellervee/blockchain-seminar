var Rating = require('./models/rating');

exports.getRatings = function (req, res) {
  Rating.find({property_id : req.params.property_id}, function(err, property) {
    if (err)
      res.send(err);
    res.json(property);
  });
}

exports.addRating = function (req, res) {
  Rating.create(
    { property_id: req.body.property_id,
      username: req.body.username,
      rating: req.body.rating
    },
    function(err, property) {
      if (err)
        res.send(err);
      res.json(property);
    });
}
