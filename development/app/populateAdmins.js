var User = require('./models/user');
var UserController = require ('./userController');

module.exports = function(app) {
  // Andreas
  var query = {
    username: 'andreas'
  };
  var update = {
    username: 'andreas',
    fullName: 'Andreas Ellervee',
    email: 'test@test.test',
    cellphone: '123456789',
    password: 'andreas',
    role: 'Admin'
  };
  var options = {
    upsert: true,
    new: true,
    setDefaultsOnInsert: true
  };
  User.findOneAndUpdate(query, update, options, function(error, result) {
    if (!error) {
      console.log('Andreas admin account added');
    }
  });

  // Orlenys
  query.username = 'orlenys';
  update.username = 'orlenys';
  update.fullName = 'Orlenys Pintado';
  update.password = 'orlenys';
  User.findOneAndUpdate(query, update, options, function(error, result) {
    if (!error) {
      console.log('Orlenys admin account added');
    }
  });
}
