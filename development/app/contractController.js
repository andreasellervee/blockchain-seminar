var Contract = require('./models/contract');

exports.getContracts = function (req, res){
	Contract.find(
		function(err, contract) {
			if (err)
				res.send(err)
			res.json(contract);
		}
  );
}

exports.getEnabledContracts = function (req, res){
	Contract.find({ available: req.params.enabled },
		function(err, contract) {
			if (err)
				res.send(err)
			res.json(contract);
		}
  );
}

exports.insertContract = function(req, res) {
		Contract.create(
      { client: req.body.client,
        host: req.body.host,
        property: req.body.property,
        status: req.body.status,
        contractAddress: req.body.contractAddress,
        clientAddress: req.body.clientAddress,
        hostAddress: req.body.hostAddress,
        meetingDate: req.body.meetingDate,
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        available: true },
			function(err, contract) {
				if (err)
					res.send(err);
        res.json(contract);
		  });
	}

exports.updateContract = function(req, res){
	Contract.update( {_id : req.params.contract_id},
					{$set:{ client: req.body.client,
                  host: req.body.host,
                  property: req.body.property,
                  status: req.body.status,
                  contractAddress: req.body.contractAddress,
                  clientAddress: req.body.clientAddress,
                  hostAddress: req.body.hostAddress,
                  meetingDate: req.body.meetingDate,
                  startDate: req.body.startDate,
                  endDate: req.body.endDate,
                  available: req.body.available
          }},
					function(err, contract) {
						if (err)
							res.send(err);
            res.json(contract);
			});
}

exports.getHost = function(req, res) {
	Contract.aggregate(
		[
			{$match:
				{host: req.params.host_id}
			},
			{$lookup:
				{
					from: "properties",
					localField: "property",
					foreignField: "_id",
					as: "contractProperty"}
			}
		], function(err, contract) {
  		if (err)
  			res.send(err);
  		res.json(contract);
  	});
}

exports.getClient = function(req, res) {
		Contract.aggregate(
			[
				{$match:
					{client: req.params.client_id}
				},
				{$lookup:
					{
						from: "properties",
						localField: "property",
						foreignField: "_id",
						as: "contractProperty"}
				}
			], function(err, contract) {
	  		if (err)
	  			res.send(err);
	  		res.json(contract);
	  	});
}

exports.getContract = function(req, res) {
  	Contract.findOne({_id : req.params.contract_id}, function(err, contract) {
  		if (err)
  			res.send(err);
  		res.json(contract);
  	});
}

exports.removeContract = function(req, res) {
	Contract.remove({_id : req.params.contract_id}, function(err, contract) {
		if (err)
			res.send(err);
    res.json(contract);
	});
}
