var User = require('./models/user');

exports.getUsers = function (req, res){
	User.find(
		function(err, user) {
			if (err)
				res.send(err)
			res.json(user);
		});
}

exports.insertUser = function(req, res) {
		User.create(
      { username: req.body.username,
        fullName: req.body.fullName,
        email: req.body.email,
        cellphone: req.body.cellphone,
        password: req.body.password,
        role: req.body.role },
			function(err, user) {
				if (err)
					res.send(err);
				res.json(user);
		  });
	}

exports.updateUser = function(req, res){
	User.update( {_id : req.params.user_id},
					{$set:{ username: req.body.username,
                  fullName: req.body.fullName,
                  email: req.body.email,
                  cellphone: req.body.cellphone,
                  password: req.body.password,
                  role: req.body.role }},
					function(err, user) {
						if (err)
							res.send(err);
            res.json(user);
			});
}

exports.getUser = function(req, res) {
  	User.find({username: req.params.username}, function(err, user) {
  		if (err)
  			res.send(err);
  		res.json(user);
  	});
}

exports.removeUser = function(req, res) {
	User.remove({_id : req.params.user_id}, function(err, user) {
		if (err)
			res.send(err);
    res.json(user);
	});
}
