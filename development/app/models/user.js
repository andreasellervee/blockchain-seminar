
var mongoose = require('mongoose');

module.exports = mongoose.model('User', {
  username: String,
  fullName: String,
  email: String,
  cellphone: Number,
  password: String,
  role: String
});
