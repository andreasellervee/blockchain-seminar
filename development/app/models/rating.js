
var mongoose = require('mongoose');

module.exports = mongoose.model('Rating', {
  property_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Property'},
  username: String,
  rating: Number
});
