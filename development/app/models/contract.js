var mongoose = require('mongoose');

module.exports = mongoose.model('Contract', {
  client: String,
  host: String,
  property: {type: mongoose.Schema.Types.ObjectId, ref: 'Property'},
  status: String,
  contractAddress: String,
  clientAddress: String,
  hostAddress: String,
  meetingDate: Date,
  startDate: Date,
  endDate: Date,
  available: Boolean
});
