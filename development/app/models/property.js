
var mongoose = require('mongoose');

module.exports = mongoose.model('Property', {
  city: String,
  location: String,
  cost: Number,
  area: Number,
  floorlocation: Number,
  services: String,
  availablefrom: Date,
  availableuntil: Date,
  reserved: Boolean,
  host: String
});
