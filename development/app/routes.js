var User = require('./models/user');
var Property = require('./models/property');
var Contract = require('./models/contract');

var UserController = require ('./userController');
var PropertyController = require ('./propertyController');
var ContractController = require ('./contractController');
var RatingController = require('./ratingController');

module.exports = function(app) {

  // Users Routes

  app.get('/api/user-list', UserController.getUsers);

  app.get('/api/user/:username', UserController.getUser);

  app.post('/api/user', UserController.insertUser);

  app.put('/api/user/:id', UserController.updateUser);

  app.delete('/api/user/:id', UserController.removeUser);

  // Properties Routes

  app.get('/api/property-list', PropertyController.getproperties);

  app.get('/api/property/:property_id', PropertyController.getProperty);

  app.post('/api/property', PropertyController.insertProperty);

  app.put('/api/property/:property_id', PropertyController.updateProperty);

  app.delete('/api/property/:property_id', PropertyController.removeProperty);

  // Contract Routes

  app.get('/api/contract-list', ContractController.getContracts);

  app.get('/api/contract-list/:enabled', ContractController.getEnabledContracts);

  app.get('/api/contract/:contract_id', ContractController.getContract);

  app.get('/api/contract_host/:host_id', ContractController.getHost);

  app.get('/api/contract_client/:client_id', ContractController.getClient);

  app.post('/api/contract', ContractController.insertContract);

  app.put('/api/contract/:contract_id', ContractController.updateContract);

  app.delete('/api/contract/:contract_id', ContractController.removeContract);

  // Rating routes

  app.get('/api/rating/:property_id', RatingController.getRatings);
  
  app.post('/api/rating', RatingController.addRating);


	app.get('*', function(req, res) {
		res.sendfile('./dist/index.html'); // Carga única de la vista
	});
};
