var Property = require('./models/property');

exports.getproperties = function (req, res){
	Property.aggregate([
		{$match: {reserved : false }},
		{$lookup: {
			from: 'ratings',
			localField: '_id',
			foreignField: 'property_id',
			as: 'propertyRatings'
		}}
	], function(err, property) {
		if (err)
			res.send(err)
		res.json(property);
	});
}

exports.insertProperty = function(req, res) {
		Property.create(
      { city: req.body.city,
				location: req.body.location,
				cost: req.body.cost,
				area: req.body.area,
				floorlocation: req.body.floorlocation,
				services: req.body.services,
				availablefrom: req.body.availablefrom,
				availableuntil: req.body.availableuntil,
				host: req.body.host,
				reserved: req.body.reserved },
			function(err, property) {
				if (err)
					res.send(err);
        res.json(property);
		  });
	}

exports.updateProperty = function(req, res){
	Property.update( {_id : req.params.property_id},
					{$set:{ city: req.body.city,
                  location: req.body.location,
                  cost: req.body.cost,
                  area: req.body.area,
                  floorlocation: req.body.floorlocation,
                  services: req.body.services,
                  availablefrom: req.body.availablefrom,
                  availableuntil: req.body.availableuntil,
                  host: req.body.host,
                  reserved: req.body.reserved }},
					function(err, property) {
						if (err)
							res.send(err);
            res.json(property);
			});
}

exports.getProperty = function(req, res) {
  	Property.findOne({_id : req.params.property_id}, function(err, property) {
  		if (err)
  			res.send(err);
  		res.json(property);
  	});
}

exports.removeProperty = function(req, res) {
	Property.remove({_id : req.params.property_id}, function(err, property) {
		if (err)
			res.send(err);
    res.json(property);
		});
}
