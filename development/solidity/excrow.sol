pragma solidity ^0.4.0;
contract Escrow {
    address client;
    address host;

    uint clientDeposit;
    uint hostDeposit;
    uint rentalPrice;

    uint state = 1;

    // 1-> 2
    function setRentalPrice(uint _rentalPrice) payable {
        if(state != 1 || _rentalPrice <= 0) throw;
        host = msg.sender;
        rentalPrice = _rentalPrice;
        state++;
    }

    // 2-> 0
    function cancelBeforeClientTransfers() returns (bool){
        if(state != 2 || msg.sender != host) throw;
        state = rentalPrice = 0;
    }

    // 2-> 3
    function blockClientDeposit() payable {
        if (state != 2 || (3 * rentalPrice != 2 * msg.value &&  3 * rentalPrice != 2 * msg.value + 1000000000000000000)) throw;
        client = msg.sender;
        clientDeposit = msg.value;
        state++;
    }

    // 3-> 0
    function cancelBeforeHostTransfers() returns (bool){
        if(state != 3 || msg.sender != client) throw;
        if (!client.send(clientDeposit)) throw;
        state = clientDeposit = 0;
    }

    // 3-> 4
    function blockHostDeposit() payable {
        if (state != 3 || host != msg.sender || (rentalPrice != 2 * msg.value && rentalPrice != 2 * msg.value + 1000000000000000000)) throw;
        hostDeposit = msg.value;
        state++;
    }

    // 4-> 0
    function userCancelation() {
      if(state != 4 || msg.sender != client) throw;
      transactBoth(clientDeposit - hostDeposit / 2, hostDeposit + hostDeposit / 2, 0, 0, 0);
    }

   // 4-> 0
   function hostCancelation() {
      if(state != 4 || msg.sender != host) throw;
      transactBoth(clientDeposit + hostDeposit / 2, hostDeposit / 2, 0, 0, 0);
   }

   // 5-> 0
   function processClientViolation() {
       if(state != 5) throw;
       transactBoth(rentalPrice, 2 * hostDeposit, 0, 0, 0);
   }

   // 5-> 0
   function processHostViolation() {
       if(state != 5) throw;
       if (!client.send(clientDeposit + hostDeposit)) throw;
       state = hostDeposit = clientDeposit = 0;
   }

   // 5-> 6
   function processContractTransaction(){
       if(state != 5) throw;
       if (!host.send(rentalPrice + hostDeposit)) throw;
       clientDeposit -= rentalPrice;
       hostDeposit = 0;
       state++;
   }

   // 4-> 5
   function processTimeEvent() {
       state++;
   }

   // Include both posibilities, client keep his deposit or host get the deposit after a client violation.
   function processContractConclusion(){
       if(state != 6 || (msg.sender != client && msg.sender != host)) throw;
       if (!msg.sender.send(clientDeposit)) throw;
       state = clientDeposit = 0;
   }

    function transactBoth(uint _clientAmount, uint _hostAmount, uint _clientRemainder, uint _hostRemainder, uint _state) internal {
       if (!client.send(_clientAmount) || !host.send(_hostAmount)) throw;
       hostDeposit = _hostRemainder;
       clientDeposit = _clientRemainder;
       state = _state;
   }
}
